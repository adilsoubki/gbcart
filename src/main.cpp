#include "WProgram.h"
#include "rom.h"

//    PORT  BIT  PIN
// 1     A    5   25
// 2     A   12    3
// 3     A   13    4
// 4     A   14   26
// 5     A   15   27
// 6     A   16   28
// 7     A   17   39
// 8     A   26   42
// 9     A   28   40
// 10    A   29   41
// 1     B    0   16
// 2     B    1   17
// 3     B    2   19
// 4     B    3   18
// 5     B    4   49
// 6     B    5   50
// 7     B   10   31
// 8     B   11   32
// 9     B   16    0
// 10    B   17    1
// 11    B   18   29
// 12    B   19   30
// 13    B   20   43
// 14    B   21   46
// 15    B   22   44
// 16    B   23   45
// 1     C    0   15
// 2     C    1   22
// 3     C    2   23
// 4     C    3    9
// 5     C    4   10
// 6     C    5   13
// 7     C    6   11
// 8     C    7   12
// 9     C    8   35
// 10    C    9   36
// 11    C   10   37
// 12    C   11   38
// 1     D    0    2
// 2     D    1   14
// 3     D    2    7
// 4     D    3    8
// 5     D    4    6
// 6     D    5   20
// 7     D    6   21
// 8     D    7    5
// 9     D    8   47
// 10    D    9   48
// 11    D   11   55
// 12    D   12   53
// 13    D   13   52
// 14    D   14   51
// 15    D   15   54
// 1     E    0   58
// 2     E    1   59
// 3     E    2   60
// 4     E    3   61
// 5     E    4   62
// 6     E    5   63
// 7     E   10   56
// 8     E   11   57
// 9     E   24   33
// 10    E   25   34
// 11    E   26   24

uint8_t CLOCK_PIN = 24;
uint8_t DATA_PINS[] = {12, 11, 13, 10, 9, 23, 22, 15}; // PORT C [7 - 0]
uint8_t ADDR_PINS[] = {27, 26, 4, 3,               // PORT A [15 - 12]
                       38, 37, 36, 35,             // PORT C [11 - 8]
                       5, 21, 20, 6, 8, 7, 14, 2}; // PORT D [7 - 0]

// TODO: assert no writes happen to the ram space.
void onClock()
{
    // Wait for the address to be ready.
    // asm("NOP"); asm("NOP"); asm("NOP"); asm("NOP"); asm("NOP");

    uint16_t addr = (GPIOA_PDIR & 0xF000) | (GPIOC_PDIR & 0x0F00) | (GPIOD_PDIR & 0x00FF);
    if (addr < 0x8000) {
        GPIOC_PDOR = ROM[addr];
    } else {
        GPIOC_PDOR = 0x00;
    }

    // Wait for the gameboy to read the data.
    // asm("NOP"); asm("NOP"); asm("NOP"); asm("NOP"); asm("NOP");
}

void setup()
{
    Serial.begin(115200);
    pinMode(CLOCK_PIN, INPUT_PULLDOWN);
    for (uint8_t pin : DATA_PINS) {
        pinMode(pin, OUTPUT);
    }
    for (uint8_t pin : ADDR_PINS) {
        pinMode(pin, INPUT_PULLDOWN);
    }
    attachInterrupt(CLOCK_PIN, onClock, RISING);
}

extern "C" int main(void)
{
    setup();
    while (1) {
    }
}
