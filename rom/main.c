#include <gb/gb.h>
#include <gb/drawing.h>

void main()
{
    DISPLAY_ON;
    NR52_REG = 0x80;
    NR50_REG = 0x77;
    NR51_REG = 0xFF;

    gotogxy(4, 8);
    gprint("hello world!");
    while (1)
    {
        if (joypad())
        {
            NR10_REG = 0x00;
            NR11_REG = 0x40;
            NR12_REG = 0x73;
            NR13_REG = 0x00;
            NR14_REG = 0xC3;
        }
    }
}
